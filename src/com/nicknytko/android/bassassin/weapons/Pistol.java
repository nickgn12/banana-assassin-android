package com.nicknytko.android.bassassin.weapons;

import com.nicknytko.android.bassassin.R;

public class Pistol extends BaseWeapon
{
	public int cooldown(){ return 0; }
	public int spread(){ return 0; }
	public int bulletsPerShoot(){ return 1; }
	public boolean automatic(){ return false; }
	public boolean infiniteAmmo(){ return true; }
	public float getRecoil(){ return 40; }
	
	//graphics thingies
	public int getTexture(){ return R.drawable.gun; }
	public int getLeftTexture(){ return R.drawable.gunleft; }
	public int xOffset(){ return 24; }
	public int xLeftOffset(){ return -10; }
	public int yOffset(){ return -10; }
	
	public int getPivotX(){ return 0; }
	public int getLeftPivotX(){ return 24; }
	public int getPivotY(){ return 10; }
	
	public int bulletXOffset(){ return 16; }
	public int bulletYOffset(){ return 16; }
}