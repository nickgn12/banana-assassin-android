package com.nicknytko.android.bassassin.weapons;

import com.nicknytko.android.bassassin.R;

public class Uzi extends BaseWeapon
{
    public Uzi()
    {
        lastShot = 0;
        ammoleft = 150;
        defaultAmmo = ammoleft;
    }

    public int cooldown(){ return 150; }
    public int spread(){ return 5; }
    public int bulletsPerShoot(){ return 1; }
    public boolean automatic(){ return true; }
    public boolean infiniteAmmo(){ return false; }
    public float getRecoil(){ return 40; }

    //graphics thingies
    public int getTexture(){ return R.drawable.uzi; }
    public int getLeftTexture(){ return R.drawable.uzileft; }
    public int xOffset(){ return 25; }
    public int xLeftOffset(){ return -16; }
    public int yOffset(){ return 0; }

    public int getPivotX(){ return 0; }
    public int getLeftPivotX(){ return 24; }
    public int getPivotY(){ return 10; }

    public int bulletXOffset(){ return 26; }
    public int bulletYOffset(){ return 20; }
}