package com.nicknytko.android.bassassin.weapons;

public class BaseWeapon
{
	public long lastShot;
	public int ammoleft;
	
	protected int defaultAmmo;
	
	public BaseWeapon()
	{
		lastShot = 0;
		ammoleft = 0;
		defaultAmmo = ammoleft;
	}
	
	public boolean canShoot(){ return ((ammoleft>0 || infiniteAmmo()) && (System.currentTimeMillis() > lastShot+cooldown())); } 
	
	public int cooldown(){ return 0; }
	public int spread(){ return 0; } //in radians
	public int bulletsPerShoot(){ return 0; } //useful for shotguns
	public boolean automatic(){ return false; }
	public boolean infiniteAmmo(){ return false; }
	public int getDefaultAmmo(){ return defaultAmmo; }
	public float getRecoil(){ return 0; }
	
	//graphics thingies
	public int getTexture(){ return 0; }
	public int getLeftTexture(){ return 0; }
	
	public int xOffset(){ return 0; }
	public int xLeftOffset(){ return -xOffset(); }
	public int yOffset(){ return 0; }
	
	public int getPivotX(){ return 0; }
	public int getLeftPivotX(){ return 0; }
	public int getPivotY(){ return 0; }
	
	public int bulletXOffset(){ return 0; }
	public int bulletYOffset(){ return 0; }
}