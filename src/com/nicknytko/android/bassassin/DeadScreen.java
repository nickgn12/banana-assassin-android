package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class DeadScreen extends Gamemode
{
	public BananaBitmap deathscreen;
	public float alpha;
	public long timeStarted;
		
	public void initialize()
	{
		deathscreen = Api.getGraphicsSubsystem().loadImage(R.drawable.dead);
		alpha = 255;
		timeStarted = System.currentTimeMillis();
	}
	
	public void onTick()
	{
		if (System.currentTimeMillis() > timeStarted + 5000){ Api.getGame().changeGamemode(new Leaderboard()); }
	}
	
	public void onDraw( GraphicsSubsystem g )
	{
		g.clearScreen(50, 50, 50);
		g.drawBitmap(deathscreen, g.getScreenWidth()/2 - deathscreen.getWidth()/2,  g.getScreenHeight()/2 - deathscreen.getHeight()/2);
		
		alpha *= 0.96;
		
		if (alpha > 1)
		{
			g.setColor(255, 0, 0, (int)alpha);
			g.drawRectangle(0, 0, g.getScreenWidth(), g.getScreenHeight());
		}
	}
}