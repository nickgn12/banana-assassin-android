package com.nicknytko.android.bassassin.engine;

import java.util.Vector;

public class GlobalVarTable
{
	private Vector<GlobalVariable> variables;
	
	public GlobalVarTable()
	{
		variables = new Vector<GlobalVariable>();
	}
	
	private GlobalVariable variableExists( String name )
	{
		for (int i=0;i < variables.size();i++)
		{
			if (variables.get(i).getName().equalsIgnoreCase(name)){ return variables.get(i); }
		}
		
		return null;
	}
	
	public GlobalVariable createVariable( String name, int value )
	{
		GlobalVariable exists = variableExists(name);
		if (exists != null)
		{
			exists.setValue(value);
			return exists;
		}
		
		GlobalVariable temp = new GlobalVariable(name,value);
		variables.add(temp);
		
		return temp;
	}
	
	public GlobalVariable getVariable( String name )
	{
		GlobalVariable exists = variableExists(name);
		if (exists != null){ return exists; }
		
		return createVariable(name,0);
	}
	
	public void setVariable( String name, int value )
	{
		getVariable(name).setValue(value);
	}
}