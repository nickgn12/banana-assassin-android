package com.nicknytko.android.bassassin.engine;

import com.nicknytko.android.bassassin.BananaActivity;
import android.util.Log;

public class Debug
{
	public void out( String msg )
	{
		try
		{
			if (msg.equals("")){ return; }
			if (msg.equals(null)){ return; }
			Log.d(BananaActivity.class.getSimpleName(), msg);
		}
		catch (NullPointerException e)
		{
			Log.d(BananaActivity.class.getSimpleName(), "Tried to print null pointer");
		}
	}
}