package com.nicknytko.android.bassassin.engine;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class BananaNet
{
	@SuppressLint("DefaultLocale")
	public InetAddress intToIp( int ip )
	{
		String ipString = String.format(
			"%d.%d.%d.%d",
			(ip & 0xff),
			(ip >> 8 & 0xff),
			(ip >> 16 & 0xff),
			(ip >> 24 & 0xff));
				
		try {
			return InetAddress.getByName(ipString);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public InetAddress getWifiIp()
	{
		WifiManager wifiMan = (WifiManager)Api.getMainActivity().getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiMan.getConnectionInfo();
		
		return intToIp(wifiInfo.getIpAddress());
	}
}