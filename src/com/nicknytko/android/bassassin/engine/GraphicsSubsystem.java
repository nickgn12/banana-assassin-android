package com.nicknytko.android.bassassin.engine;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.graphics.Canvas;
import android.graphics.drawable.NinePatchDrawable;

public class GraphicsSubsystem
{
	private float density;
	private int densityInt;
	
	private int screenWidth;
	private int screenHeight;
	
	private int screenActualWidth;
	private int screenActualHeight;
	
	private Bitmap.Config bitmapConfig;
	private GraphicsLoader loader;
	
	private Canvas drawing;
	private Paint drawingPaint;
	private Paint fontPaint;
	
	public int pixelToDp( int px )
	{
		if (densityInt == 200){ return px/2; }
		if (densityInt == 100){ return px; }
		return (px * 100) / densityInt;
	}
	
	public int dpToPixel( int dp )
	{
		//return dp;
		//if (densityInt == 200){ return dp*2; }
		//if (densityInt == 100){ return dp; }
		return (dp * densityInt) / 100;
	}
			
	public GraphicsSubsystem( )
	{
		DisplayMetrics dm = Api.getMainActivity().getResources().getDisplayMetrics();
		density = dm.density;
		densityInt = (int)(density*100.0f);
		
		Api.getDebug().out("Screen density is " + density + ".");
		
		screenActualWidth = dm.widthPixels;
		screenActualHeight = dm.heightPixels;
		
		Api.getDebug().out("Screen is " + screenActualWidth + "x" + screenActualHeight + " pixels.");
		
		screenWidth = pixelToDp(screenActualWidth);
		screenHeight = pixelToDp(screenActualHeight);
		
		Api.getDebug().out("Screen is " + screenWidth + "x" + screenHeight + " dp.");
		
		bitmapConfig = Bitmap.Config.ARGB_8888;
		loader = new GraphicsLoader(this);
		
		drawing = null;
		drawingPaint = new Paint();
		drawingPaint.setARGB(255, 255, 255, 255);
		fontPaint = new Paint();
		fontPaint.setARGB(255,0,0,0);
	}
	
	public float getDensity(){ return density; }
	public int getScreenWidth(){ return screenWidth; }
	public int getScreenHeight(){ return screenHeight; }
	public int getActualScreenWidth(){ return screenActualWidth; }
	public int getActualScreenHeight(){ return screenActualHeight; }
	public Bitmap.Config getBitmapConfig(){ return bitmapConfig; }
	
	public void setCanvas( Canvas drawing ){ this.drawing = drawing; }
	public Canvas getCanvas( ){ return drawing; }
	
	public BananaBitmap loadImage( int id ){ return loader.getImage(id); }
	
	public void setColor( int r, int g, int b ){ drawingPaint.setARGB(drawingPaint.getAlpha(), r, g, b); }
	public void setColor( int r, int g, int b, int a ){ drawingPaint.setARGB(a, r, g, b); }
	public void setAlpha( int a ){ drawingPaint.setAlpha(a); }
	
	public void setFontColor( int r, int g, int b ){ fontPaint.setARGB(255, r, g, b); }
	public void setFontSize( int size ){ fontPaint.setTextSize( dpToPixel(size) ); }
	public void setFontStyle( Paint.Style style ){ fontPaint.setStyle( style ); }
	public void setFontStrokeWidth( int size ){ fontPaint.setStrokeWidth( dpToPixel(size) ); }
	public void setFontFace( Typeface f ){ fontPaint.setTypeface(f); }
	public void setDefaultFontFace(){ fontPaint.setTypeface(null); }
	public void setFontPaint( Paint p ){ fontPaint = p; }
	public Paint getFontPaint(){ return fontPaint; }

	public void drawBitmap( BananaBitmap bmp, int x, int y )
	{
		drawing.drawBitmap(bmp.getImgData(), dpToPixel(x), dpToPixel(y), null);
	}
	
	public void drawRotatedBitmap( BananaBitmap bmp, int x, int y, float degrees, int pivotX, int pivotY )
	{
		x = dpToPixel(x);
		y = dpToPixel(y);
		pivotX = dpToPixel(pivotX);
		pivotY = dpToPixel(pivotY);
		
		Matrix m = new Matrix();
		m.postRotate(degrees,pivotX,pivotY);
		m.postTranslate(x, y);
		
		drawing.drawBitmap(bmp.getImgData(), m, null);
	}
	
	public void drawNinePatch( NinePatchDrawable patch, int x, int y, int width, int height )
	{
		Rect r = new Rect(dpToPixel(x),dpToPixel(y),dpToPixel(x+width),dpToPixel(y+height));
		patch.setBounds(r);
		patch.draw(drawing);
	}
	
	public void drawRectangle( int x1, int y1, int width, int height )
	{
		int actualX = dpToPixel(x1);
		int actualY = dpToPixel(y1);
		
		drawing.drawRect(actualX, actualY, actualX+dpToPixel(width), actualY+dpToPixel(height), drawingPaint);
	}
	
	public void drawText( int x, int y, String str )
	{
		fontPaint.setTextAlign(Paint.Align.LEFT);
		drawing.drawText( str, dpToPixel(x), dpToPixel(y), fontPaint );
	}
	
	public void drawCenterText( int x, int y, String str )
	{
		fontPaint.setTextAlign(Paint.Align.CENTER);
		drawing.drawText( str, dpToPixel(x), dpToPixel(y), fontPaint );
	}
	
	public void drawRightText( int x, int y, String str )
	{
		fontPaint.setTextAlign(Paint.Align.RIGHT);
		drawing.drawText( str, dpToPixel(x), dpToPixel(y), fontPaint );
	}
	
	public void clearScreen()
	{
		drawing.drawARGB(255,0,0,0);
	}
	
	public void clearScreen( int r, int g, int b )
	{
		drawing.drawARGB(255,r,g,b);
	}
}