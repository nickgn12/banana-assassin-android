package com.nicknytko.android.bassassin.engine;

import java.util.Random;

public class BananaMath
{
	public static float TWOPI = 6.28318530718f;
	public static float PI = 3.14159265359f;
	public static float HALFPI = 1.57079632679f;
	public static float PIUNDER180 = 57.2957795131f;
    public static float PIOVER180 = 0.01745329251f;
	
	public static Random rand = new Random();
	
	public static float square( float num )
	{
		return num*num;
	}
	
	public static float max( float f1, float f2 )
	{
		if (f1 > f2){ return f1; }
		return f2;
	}
	
	public static float min( float f1, float f2 )
	{
		if (f1 < f2){ return f1; }
		return f2;
	}
	
	public static float getDistance( float x1, float y1, float x2, float y2 )
	{
		return (float)Math.sqrt( square(x2 - x1) + square(y2 - y1) );
	}
	
	public static boolean pointIsInRect( float x1, float y1, int w, int h, float x2, float y2)
	{
		if (x2 < x1){ return false; }
		if (x2 > x1+w){ return false; }
		if (y2 < y1){ return false; }
		if (y2 > y1+h){ return false; }

        return true;
	}
	
	public static float getAngle( float x1, float y1, float x2, float y2 )
	{
		return (float)Math.atan2((y2-y1), (x2-x1));
	}
	
	public static float getAngleDegrees( float x1, float y1, float x2, float y2 )
	{
		return getAngle(x1,y1,x2,y2) * PIUNDER180;
	}

    public static float floor( float a )
    {
        return (float)((int)a);
    }

    public static int randInt( int low, int high )
    {
        return (rand.nextInt(high - low)) + low;
    }
}