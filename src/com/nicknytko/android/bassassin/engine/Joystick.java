package com.nicknytko.android.bassassin.engine;

import java.util.Vector;

import com.nicknytko.android.bassassin.R;
import android.view.MotionEvent;

public class Joystick
{
	private int joystickDragging;
	private float joyx,joyy;

	public BananaBitmap imgJoystick;
	public BananaBitmap imgJoyTop;
	
	private int scrWidth, scrHeight;
	private Vector<JoystickButton> buttons;
	
	
	public float getJoyX()
	{
		return joyx / 50.0f;
	}
	
	public float getJoyY()
	{
		return joyy / 50.0f;
	}
	
	public void clearButtons()
	{
		buttons.clear();
	}
	
	public void addButton( JoystickButton b )
	{
		buttons.add(b);
	}
	
	public Joystick( )
	{
		imgJoystick = Api.getGraphicsSubsystem().loadImage(R.drawable.joystick);
		imgJoyTop = Api.getGraphicsSubsystem().loadImage(R.drawable.joytop);
		
		scrWidth = Api.getGraphicsSubsystem().getScreenWidth();
		scrHeight = Api.getGraphicsSubsystem().getScreenHeight();
		
		joystickDragging = -1;
		joyx = joyy = 0;
		
		buttons = new Vector<JoystickButton>();
	}
	
	public void onTick()
	{
		for (int i=0;i < buttons.size();i++)
		{
			buttons.get(i).isPressed = false;
            buttons.get(i).isReleased = false;
		}
	}
	
	public void onDraw( GraphicsSubsystem g )
	{
		g.drawBitmap(imgJoystick, 40, g.getScreenHeight() - 30 - imgJoystick.getHeight() );
		g.drawBitmap(imgJoyTop,40 + (int)(imgJoystick.getWidth()/2 - imgJoyTop.getWidth()/2) + (int)(joyx*10), g.getScreenHeight() - 30 - imgJoystick.getHeight() + (int)(imgJoystick.getHeight()/2 - imgJoyTop.getHeight()/2) + (int)(joyy*10) );
	
		for (int i=0;i < buttons.size();i++)
		{
			g.drawBitmap(buttons.get(i).getBitmap(), buttons.get(i).x, buttons.get(i).y);
		}
	}
	
	public boolean onTouch( MotionEvent event )
	{
		int actionIndex = event.getActionIndex();
		//int actionCode = event.getAction() & MotionEvent.ACTION_MASK;
		int actionCode = event.getActionMasked();	
		
		int actionX = Api.getGraphicsSubsystem().pixelToDp((int)event.getX(actionIndex));
		int actionY = Api.getGraphicsSubsystem().pixelToDp((int)event.getY(actionIndex));
		
		if ((actionCode == MotionEvent.ACTION_UP || actionCode == MotionEvent.ACTION_POINTER_UP) && event.getActionIndex()==joystickDragging)
		{
			joystickDragging = -1;
			joyx = joyy = 0;
			return true;
		}
        if (actionCode == MotionEvent.ACTION_UP || actionCode == MotionEvent.ACTION_POINTER_UP)
        {
            for (int i=0;i < buttons.size();i++)
            {
                if (buttons.get(i).pointerId == event.getActionIndex())
                {
                    buttons.get(i).pointerId = -1;
                    buttons.get(i).isDown = false;
                    buttons.get(i).isReleased = true;
                }
            }
        }
		if (actionX < scrWidth/2)
		{
			if (actionCode == MotionEvent.ACTION_DOWN || actionCode == MotionEvent.ACTION_POINTER_DOWN)
			{
				if (BananaMath.getDistance(90,(scrHeight - 30 - imgJoystick.getHeight() + 50),actionX, actionY) <= imgJoystick.getWidth())
				{
					joystickDragging = event.getActionIndex();
					return true;
				}
				return false;
			}
			if (actionCode == MotionEvent.ACTION_MOVE && (event.getActionIndex()==joystickDragging))
			{
				float movex = actionX - (40 + imgJoystick.getWidth()/2);
				float movey = actionY - (scrHeight - 30 - imgJoystick.getHeight()/2);
				
				if (movex < -50){ movex = -50; }
				if (movex > 50){ movex = 50; }
				
				if (movey < -50){ movey = -50; }
				if (movey > 50){ movey = 50; }
				
				joyx = movex/10;
				joyy = movey/10;
				return false;
			}
		}
		
		if (actionCode == MotionEvent.ACTION_DOWN || actionCode == MotionEvent.ACTION_POINTER_DOWN)
		{
			for (int i=0;i < buttons.size();i++)
			{
				if (buttons.get(i).isPressed(actionX, actionY, event.getSize(actionIndex)))
                {
                    buttons.get(i).isPressed = true;
                    buttons.get(i).isDown = true;
                    buttons.get(i).pointerId = event.getActionIndex();
                    return true;
                }
			}
		}
		return false;
	}
}