package com.nicknytko.android.bassassin.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileIO
{
	public boolean fileExists( String filename )
	{
		File temp = Api.getMainActivity().getFileStreamPath(filename);
		return temp.exists();
	}
	
	public FileOutputStream loadFileAsOutput( String filename )
	{
		try
		{
			File temp = Api.getMainActivity().getFileStreamPath(filename);
			if (temp.exists()){ temp.delete(); temp.createNewFile(); }
			else { temp.createNewFile(); }
			
			return new FileOutputStream(temp);
			//return Api.getMainActivity().openFileOutput(filename, Context.MODE_PRIVATE);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public FileInputStream loadFileAsInput( String filename )
	{
		try
		{
			return Api.getMainActivity().openFileInput(filename);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}