package com.nicknytko.android.bassassin.engine;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MainThread extends Thread
{
	//controls what actually happens
	public boolean running;
	private SurfaceHolder surfaceHolder;
	private final int MAX_FPS = 60;
	private final float FRAME_TIME = (1000.0f / (float)MAX_FPS);
	private Game game;
	
	public void initialize( SurfaceHolder surfaceHolder )
	{
		this.surfaceHolder = surfaceHolder;
		game = new Game();
		Api.registerGame(game);
		
		game.initialize();
	}
	
	public void deinitialize()
	{
	}
	
	public boolean onTouch( MotionEvent event )
	{
		return game.onTouch(event);
	}
	
	public void onDraw( Canvas c )
	{
		Api.getGraphicsSubsystem().setCanvas(c);
		game.onDraw(Api.getGraphicsSubsystem());
	}
	
	@Override
	public void run()
	{
		Api.getDebug().out("Started thread");
		
		Canvas c = null;
		long startTime, endTime;	
		
		while (running)
		{
			try
			{
				c = surfaceHolder.lockCanvas();
				synchronized (surfaceHolder)
				{
					startTime = System.currentTimeMillis();

                    Api.incrementTicks();
					game.onTick();
	
					if (c != null)
					{						
						Api.getGraphicsSubsystem().setCanvas(c);
						game.onDraw(Api.getGraphicsSubsystem());
					}
					
					endTime = System.currentTimeMillis();
										
					if (endTime < startTime+FRAME_TIME)
					{
                        try 
                        {
                        	Thread.sleep((long)(FRAME_TIME - (endTime-startTime)));
                        } 
                        catch (Exception e){ e.printStackTrace(); }
					}
				}
			}
			finally 
			{
				if (c != null)
				{
					try
					{
						surfaceHolder.unlockCanvasAndPost(c);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
}