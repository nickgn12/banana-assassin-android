package com.nicknytko.android.bassassin.engine.scene;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class Graph
{
    private BaseNode[] nodes;
    private int nodeCount;
    private int maxNodes;

    public Graph()
    {
        maxNodes = 256;
        nodeCount = 0;
        nodes = new BaseNode[maxNodes];
    }

    public void resizeNodeArray( int newSize )
    {
        Api.getDebug().out("Resizing scene graph to " + newSize + " maximum nodes!");

        BaseNode[] tempNodes = new BaseNode[newSize];

        int nodesToCopy = (int)BananaMath.min(newSize,maxNodes);
        for (int i=0;i < nodesToCopy;i++)
        {
            tempNodes[i] = nodes[i];
        }

        nodes = tempNodes;
        maxNodes = newSize;
    }

    public void shiftRight( int start )
    {
        if (start == nodeCount-1)
        {
            nodes[nodeCount] = nodes[nodeCount-1];
            nodes[nodeCount-1] = null;
        }
        else
        {
            for (int i=nodeCount;i > start;i--)
            {
                nodes[i] = nodes[i-1];
            }

            nodes[start] = null;
        }

        nodeCount++;
    }

    public int findNodeSpot( BaseNode n )
    {
        if (nodeCount == 0){ return 0; }

        for (int i=0;i < nodeCount;i++)
        {
            if (n.getDepth() < nodes[i].getDepth()){ return i; }
        }

        return -1;
    }

    public void shiftLeft( int shiftInTo )
    {
        for (int i=shiftInTo;i < nodeCount;i++)
        {
            nodes[i] = nodes[i+1];
        }

        nodeCount--;
    }

    public void sortNodes()
    {
        if (nodeCount == 0 || nodeCount == 1){ return; }

        for (int i=0;i < nodeCount;i++)
        {
            BaseNode n = nodes[i];

            if (n.dead){ shiftLeft(i); --i; continue; }

            if (n.changed)
            {
                n.changed = false;
                shiftLeft(i); //take it out
                addNode(n); //and put it where it belongs
            }
        }
    }

    public void clearNodes()
    {
        nodeCount = 0;
    }

    public void addNode( BaseNode n )
    {
        if (nodeCount == 0)
        {
            nodes[0] = n;
            nodeCount++;
        }
        else
        {
            if (nodeCount > maxNodes-10){ resizeNodeArray( maxNodes*2 ); }

            int spot = findNodeSpot( n );
            if (spot == -1)
            {
                nodes[nodeCount] = n;
                nodeCount++;
            }
            else
            {
                shiftRight( spot );
                nodes[spot] = n;
            }
        }
    }

    public void onDraw( GraphicsSubsystem g )
    {
        if (nodeCount == 0){ return; }

        if (nodeCount == 1){ nodes[0].onDraw( g ); }
        else
        {
            for (int i=0;i < nodeCount;i++)
            {
                nodes[i].onDraw(g);
            }
        }
    }
}