package com.nicknytko.android.bassassin.engine.scene;

import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class Scene
{
    private Graph objectGraph;

    public Scene()
    {
        objectGraph = new Graph();
    }

    public void onTick()
    {
        objectGraph.sortNodes();
    }

    public void onDraw( GraphicsSubsystem g )
    {
        objectGraph.onDraw(g);
    }

    public void addNode( BaseNode n )
    {
        objectGraph.addNode(n);
    }

    public void clearNodes( )
    {
        objectGraph.clearNodes();
    }
}