package com.nicknytko.android.bassassin.engine;

public class GlobalVariable
{
	private String name;
	private int value;
	
	public GlobalVariable( String name, int value )
	{
		this.name = name;
		this.value = value;
	}
	
	public int getValue(){ return value; }
	public void setValue( int newVal ){ value = newVal; }
	public String getName(){ return name; }
}