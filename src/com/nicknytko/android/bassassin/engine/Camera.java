package com.nicknytko.android.bassassin.engine;

public class Camera
{
	private int targetX, targetY;
	private float x, y;
	
	public float screenShake;
	
	private float shakeX, shakeY;
	private Entity following;
	
	public Camera()
	{
		targetX = targetY = 0;
		x = y = 0.0f;
		
		screenShake = 0.0f;
		shakeX = shakeY = 0.0f;
		
		following = null;
	}
	
	public void onTick()
	{
		if (following != null)
		{
			targetX = following.x - Api.getGraphicsSubsystem().getScreenWidth()/2;
			targetY = following.y - Api.getGraphicsSubsystem().getScreenHeight()/2;
		}
		
		if (x != targetX){ x += targetX/4; }
		if (y != targetY){ y += targetY/4; }
		
		screenShake *= 0.8;
		shakeX = (BananaMath.rand.nextFloat()*(screenShake*2)) - screenShake;
		shakeY = (BananaMath.rand.nextFloat()*(screenShake*2)) - screenShake;
	}
	
	public int getX(){ return (int)x - (int)shakeX; }
	public int getY(){ return (int)y - (int)shakeY; }
	
	public void setX( int setx ){ targetX = setx; }
	public void setY( int sety ){ targetY = sety; }
	
	public void startFollowingEntity( Entity e ){ following = e; }
	public void stopFollowingEntity(){ following = null; }
}