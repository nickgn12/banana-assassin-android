package com.nicknytko.android.bassassin.engine;

import com.nicknytko.android.bassassin.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class GraphicsLoader
{
	private BananaBitmap[] images;
	private int imagesLoaded;
	private BitmapFactory.Options factoryOptions;
	private GraphicsSubsystem parent;
	
	public GraphicsLoader( GraphicsSubsystem parent )
	{
		this.parent = parent;
		
		images = new BananaBitmap[R.drawable.class.getFields().length];
		
		Api.getDebug().out("There are " + R.drawable.class.getFields().length + " images available to load.");
		
		imagesLoaded = 0;
		
		for (int i=0;i < images.length;i++)
		{
			images[i] = new BananaBitmap();
		}
		
		factoryOptions = new BitmapFactory.Options();
		factoryOptions.inPreferredConfig = parent.getBitmapConfig();
		
		//density = parent.getDensity();
		//density = Api.getActivity().getResources().getDisplayMetrics().density;
	}
	
	private int getImageIndex( int id )
	{
		for (int i=0;i < imagesLoaded;i++)
		{
			if (id == images[i].id){ return i; }
		}
		return -1;
	}
	
	public boolean isImageLoaded( int id )
	{
		return (getImageIndex(id) != -1);
	}
	
	public int loadImage( int id )
	{
		if (isImageLoaded(id)){ return -1; }
		
		Bitmap temp = BitmapFactory.decodeResource( Api.getMainActivity().getResources(), id, factoryOptions );
		BananaBitmap btemp = new BananaBitmap(parent,temp);
		btemp.id = id;
		
		images[imagesLoaded] = btemp;
		
		++imagesLoaded;
		return imagesLoaded-1;
	}
	
	public BananaBitmap getImage( int id )
	{
		int imageid = getImageIndex(id);
		
		if (imageid == -1)
		{
			return images[loadImage(id)];
		}
		else
		{
			return images[imageid];
		}
	}
}