package com.nicknytko.android.bassassin.engine;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Vector;

public class BananaSocket extends Thread
{
	DatagramSocket socket;
	
	Vector<DatagramPacket> packets;
	public boolean running = false;
	
	public int getPort()
	{
		return socket.getLocalPort();
	}
	
	public InetAddress getIP()
	{
		return socket.getLocalAddress();
	}
	
	public boolean packetsAvailable()
	{
		return packets.size() > 0;
	}
	
	public DatagramPacket getNextPacket()
	{
		DatagramPacket d = packets.firstElement();
		packets.remove(0);
		
		return d;
	}
	
	public BananaSocket( InetAddress ip, int port )
	{
		try
		{
			socket = new DatagramSocket(port,ip);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		packets = new Vector<DatagramPacket>();
	}
	
	public BananaSocket()
	{
		try
		{
			socket = new DatagramSocket();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		packets = new Vector<DatagramPacket>();
	}
	
	public void startListening()
	{
		start();
	}
	
	public void stopListening()
	{
		end();
	}
	
	public void run()
	{
		running = true;
		
		byte[] data;
		DatagramPacket dp;
		
		while (running)
		{		
			try 
			{			
				data = new byte[255];
				dp = new DatagramPacket(data, data.length);
				
				socket.receive(dp); 
				
				packets.add(dp);
			} 
			catch (Exception e){ e.printStackTrace(); }
		}
	}
	
	public void end()
	{
		running = false;
		
		try 
		{
			String exit = "exit";
			socket.send( new DatagramPacket( exit.getBytes(), exit.length(), InetAddress.getLocalHost(), getPort()) );
		}
		catch (Exception e1) 
		{
			e1.printStackTrace();
		}
		socket.close();
		try 
		{
			join();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void sendPacket( String data, InetAddress receiver, int port )
	{
		byte[] sendData = data.getBytes();

		//Api.getDebug().out(new String(sendData));
		
		DatagramPacket dp = new DatagramPacket(sendData,data.length(),receiver,port);

		try {
			socket.send(dp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void waitUntilLoaded()
	{
		while (!running) {}
	}
}