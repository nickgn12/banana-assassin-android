package com.nicknytko.android.bassassin.engine;

public class JoystickButton
{
	public int x, y;
	private BananaBitmap image;
	
	public boolean isPressed;
    public boolean isDown;
    public boolean isReleased;

    public int pointerId;
	
	public JoystickButton( int x, int y, int imageid )
	{
		this.x = x;
		this.y = y;
		image = Api.getGraphicsSubsystem().loadImage(imageid);

		isPressed = false;
        isDown = false;
        isReleased = false;

        pointerId = -1;
	}
	
	public float getDiameter()
	{
		return (int)BananaMath.max(image.getWidth(), image.getHeight());
	}
	
	public float getRadius()
	{
		return getDiameter()/2;
	}
	
	public BananaBitmap getBitmap(){ return image; }
	
	public boolean isPressed( int touchx, int touchy, float touchRadius )
	{
		float dist = BananaMath.getDistance(touchx, touchy, x + image.getWidth()/2, y + image.getHeight()/2);
		
		return (dist <= touchRadius+getRadius());
	}
}