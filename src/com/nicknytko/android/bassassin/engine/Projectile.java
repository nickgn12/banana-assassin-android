package com.nicknytko.android.bassassin.engine;

public class Projectile
{
	public int x, y;
	public int width = 0;
	public int height = 0;
	public boolean dead = false;
	public Entity owner = null;
	
	public void initialize(){}
	public void onTick(){}
	public void onDraw( GraphicsSubsystem g ){}
	public void onCollide( Entity other ){}
	public void onDie(){}
	
	public void markForDelete(){ dead = true; }
}