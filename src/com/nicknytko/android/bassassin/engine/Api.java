package com.nicknytko.android.bassassin.engine;

import android.content.pm.ActivityInfo;
import android.view.Window;
import android.view.WindowManager;

import com.nicknytko.android.bassassin.BananaActivity;
import com.nicknytko.android.bassassin.engine.scene.Scene;

public class Api
{
	private static BananaActivity bactivity;
	private static BananaView bview;
	private static MainThread mthread;
	private static GraphicsSubsystem graphics;
	private static Game curGame;
	private static Debug debug;
	private static boolean showingJoystick;
	private static FileIO fileIo;
	private static GlobalVarTable globalVars;
	private static Camera camera;
    private static long ticks;
	private static BananaNet net;
    private static Scene scene;

	public static BananaActivity getMainActivity(){ return bactivity; }
	public static BananaView getMainView(){ return bview; }
	public static MainThread getMainThread(){ return mthread; }
	public static GraphicsSubsystem getGraphicsSubsystem(){ return graphics; }
	public static Game getGame(){ return curGame; }
	public static Debug getDebug(){ return debug; }
	public static FileIO getFileIO(){ return fileIo; }
	public static boolean joystickIsShowing(){ return showingJoystick; }
	public static GlobalVarTable getGlobalVars(){ return globalVars; }
	public static Joystick getJoystick(){ return curGame.getJoystick(); }
	public static Camera getCamera(){ return camera; }
	public static long getTicks(){ return ticks; }
    public static BananaNet getNet(){ return net; }
    public static Scene getScene(){ return scene; }

	public static void registerMainActivity( BananaActivity bactivity ){ Api.bactivity = bactivity; }
	public static void registerMainView( BananaView bview ){ Api.bview = bview; }
	public static void registerMainThread( MainThread mthread ){ Api.mthread = mthread; }
	public static void registerGraphicsSubsystem( GraphicsSubsystem graphics ){ Api.graphics = graphics; }
	public static void registerGame( Game curGame ){ Api.curGame = curGame; }
	public static void showJoystick( boolean show ){ showingJoystick = show; }
    public static void incrementTicks(){ ticks++; }
	
	public static void simulateBackButton( ){ curGame.handleBackButton(); }
	
	public static void initEngine()
	{
		if (bactivity == null){ return; }
		bview = new BananaView(bactivity);
		
		debug = new Debug();
		showingJoystick = false;
		fileIo = new FileIO();
		globalVars = new GlobalVarTable();
		camera = new Camera();
        net = new BananaNet();
        scene = new Scene();

        ticks = 0;

        bactivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bactivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        bactivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		bactivity.setContentView(bview);
	}
	
	public static void quit(){ bactivity.finish(); }
}