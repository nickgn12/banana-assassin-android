package com.nicknytko.android.bassassin.engine;

public class Entity
{
	public int x, y, z;
	public int width = 0;
	public int height = 0;
	public boolean dead = false;
	
	public void initialize(){}
	public void onTick(){}
	public void onDraw( GraphicsSubsystem g ){}
	public void onCollide( Entity other ){}
	public void onCollide( Projectile other ){}
	public void onDie(){}
	public boolean canCollide(){ return false; }
	public boolean canCollideWithSelf(){ return false; }
	
	public void markForDelete(){ dead = true; }
}