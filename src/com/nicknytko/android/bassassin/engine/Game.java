package com.nicknytko.android.bassassin.engine;

import com.nicknytko.android.bassassin.MainMenu;

import android.view.MotionEvent;

public class Game
{
	private Gamemode currentMode;
	private Gamemode changeGamemodeRequest;
	private Joystick js;
	
	private Entity[] entityArray;
	private int entCount;
	private Projectile[] projectileArray;
	private int proCount; //projectile count

	public void handleBackButton()
	{
		if (currentMode != null){ currentMode.onBackButton(); }
	}
	
	public void initialize()
	{
		currentMode = new MainMenu();
		currentMode.initialize();
		changeGamemodeRequest = null;
		
		js = new Joystick();
		
		entityArray = new Entity[512];
		entCount = 0;
		
		for (int i=0;i < 512;i++)
		{
			entityArray[i] = null;
		}
		
		projectileArray = new Projectile[512];
		proCount = 0;
		
		for (int i=0;i < 512;i++)
		{
			projectileArray[i] = null;
		}
	}
	
	public void onTick()
	{
		if (changeGamemodeRequest != null)
		{
			if (currentMode != null){ currentMode.onDie(); }

            clearEntities();
			js.clearButtons();
            Api.getScene().clearNodes();
			
			currentMode = changeGamemodeRequest;
			changeGamemodeRequest = null;
			
			currentMode.initialize();
		}
		if (currentMode != null){ currentMode.onTick(); }
	
		if (Api.joystickIsShowing())
			js.onTick();
		
		for (int i=0;i < entCount;++i)
		{
			Entity e = entityArray[i];
			
			if (e.dead)
			{
				e.onDie();
				if (i == entCount){ --entCount; }
				else 
				{ 
					entityArray[i] = entityArray[entCount-1];
					entityArray[entCount-1] = null;
					--entCount;
				} //set last to this
				--i;
				continue;
			}
			
			e.onTick();
            Api.getScene().onTick();
			
			if (!e.canCollide()){ continue; }
			if (e.width == 0 || e.height == 0){ continue; }	
			
			for (int j=i;j < entCount;j++)
			{
				Entity e2 = entityArray[j];
				
				if (e2.dead){ continue; }
				if (!e2.canCollide() || e2.width == 0 || e2.height == 0){ continue; }
				if (!e.canCollideWithSelf() && (e.getClass().equals(e2.getClass()))){ continue; }
				
				if (e.x+e.width/2 < e2.x-e2.width/2){ continue; }
				if (e.y < e2.y-e2.height){ continue; }
				if (e.x-e.width/2 > e2.x+e2.width/2){ continue; }
				if (e.y-e.height > e2.y){ continue; }
				
				e.onCollide(e2);
				e2.onCollide(e);
			}
		}
		
		for (int i=0;i < proCount;i++)
		{
			Projectile p = projectileArray[i];
			
			if (p.dead)
			{
				p.onDie();
				if (i == proCount){ --proCount; }
				else 
				{ 
					projectileArray[i] = projectileArray[proCount-1];
					projectileArray[proCount-1] = null;
					--proCount;
				} //set last to this
				--i;
				continue;
			}
			
			p.onTick();
			
			for (int j=0;j < entCount;j++)
			{
				Entity e = entityArray[j];
				
				if (e.dead){ continue; }
				if (!e.canCollide() || e.width == 0 || e.height == 0){ continue; }
				if ((p.owner != null) && (p.owner == e)){ continue; }
								
				if (p.x+p.width/2 < e.x-e.width/2){ continue; }
				if (p.y+p.height/2 < e.y-e.height){ continue; }
				if (p.x-p.width/2 > e.x+e.width/2){ continue; }
				if (p.y-p.height/2 > e.y){ continue; }
				
				p.onCollide( e );
				e.onCollide( p );
			}
		}
	}
	
	public void onDraw( GraphicsSubsystem g )
	{	
		if (currentMode != null){ currentMode.preDraw(g); }
		for (int i=0;i < entCount;++i)
		{
			entityArray[i].onDraw(g);
		}

        Api.getScene().onDraw(g);

		for (int i=0;i < proCount;++i)
		{
			projectileArray[i].onDraw(g);
		}
		if (currentMode != null){ currentMode.onDraw(g); }
		if (Api.joystickIsShowing()){ js.onDraw(g); }
	}
	
	public boolean onTouch( MotionEvent event )
	{
		boolean joyTouched = false;
		if (Api.joystickIsShowing()){ joyTouched = js.onTouch(event); }

		if (joyTouched){ return true; }
		if (currentMode == null){ return false;}
		
		return currentMode.onTouch(event);
	}
	
	public void addEntity( Entity e )
	{
		if (entCount > 510){ return; }
		
		e.initialize();
		entityArray[entCount++] = e;
	}
	
	public void addProjectile( Projectile p )
	{
		if (proCount > 510){ return; }
		
		p.initialize();
		projectileArray[proCount++] = p;
	}
	
	public void clearEntities( )
	{
		for (int i=0;i < entCount;i++)
		{
			entityArray[i] = null;
		}
		
		entCount = 0;
		
		for (int i=0;i < proCount;i++)
		{
			projectileArray[i] = null;
		}
		
		proCount = 0;
	}
	
	public void changeGamemode( Gamemode newGamemode ){ changeGamemodeRequest = newGamemode; }
	public Gamemode getGamemode( ){ return currentMode; }
	
	public Joystick getJoystick(){ return js; }
}