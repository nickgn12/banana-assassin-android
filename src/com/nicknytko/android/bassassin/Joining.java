package com.nicknytko.android.bassassin;

import java.net.DatagramPacket;
import java.net.InetAddress;

import android.view.MotionEvent;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.BananaSocket;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class Joining extends Gamemode
{
	private BananaSocket sock;
	private InetAddress serverIp;
	
	private Player player;
	private int playerNum;
	
	private boolean hasPlayerNum;
	private boolean hasNumPlayers;
	
	private PlayerEntry[] players;
	
	public boolean isLoaded()
	{
		return (hasPlayerNum && hasNumPlayers);
	}
	
	public Joining( InetAddress host )
	{
		serverIp = host;
	}
	
	public void initialize()
	{
		hasPlayerNum = false;
		hasNumPlayers = false;
		playerNum = 0;
		player = new Player(false);
		
		Api.getGame().addEntity(player);
		Api.getCamera().startFollowingEntity(player);
		
		players = new PlayerEntry[15];
		for (int i=0;i < 15;i++){ players[i] = new PlayerEntry();; }
		
		Api.showJoystick(true);
		
		sock = new BananaSocket( );
		sock.startListening();
		sock.waitUntilLoaded();
		sock.sendPacket( Packet.createGenericPacket(PacketType.CONNECT), serverIp, 6790 );
	}
	public void preDraw( GraphicsSubsystem g )
	{
		g.clearScreen(255, 255, 255);
	}
	public void onDraw( GraphicsSubsystem g ){}
	public void onTick()
	{
		if (sock.packetsAvailable())
		{
			DatagramPacket p = sock.getNextPacket();
			String data = Packet.getPacketData(p);
			PacketType type = Packet.getPacketType(data);
			
			switch (type)
			{
				case PLAYERNUM:
				{
					playerNum = Packet.getBasicPacketData(data);
					
					if (players[playerNum].player != null){ players[playerNum].player.dead = true; }
					players[playerNum].player = player;
					
					hasPlayerNum = true;
					break;
				}
				case NUMPLAYERS:
				{
					int numPlayers = Packet.getBasicPacketData(data);
					
					for (int i=0;i < 15;i++)
					{
						if ((numPlayers & (1 << i)) != 0)
						{
							if (hasPlayerNum && i == playerNum){ continue; }
							
							players[i].player = new OtherPlayer();
							Api.getGame().addEntity( players[i].player );
						}
					}
					
					hasNumPlayers = true;
					break;
				}
				case PLAYERPOS:
				{
					if (!isLoaded())
						break;
					
					int moveData[] = Packet.getPlayerMoveData(data);
					if (players[moveData[0]] != null && players[moveData[0]].player != null)
					{
						players[moveData[0]].player.x = moveData[1];
						players[moveData[0]].player.y = moveData[2];
						players[moveData[0]].player.z = moveData[3];
						players[moveData[0]].player.xspeed = moveData[4];
						players[moveData[0]].player.yspeed = moveData[5];
						players[moveData[0]].player.zspeed = moveData[6]/1000;
						players[moveData[0]].player.aimAngle = moveData[7];
					}
					else
					{
						players[moveData[0]].player = new OtherPlayer();
						Api.getGame().addEntity(players[moveData[0]].player);
					}
					
					break;
				}
				default:
					break;
			}
		}
	}
	public boolean onTouch( MotionEvent event )
	{
		int actionCode = event.getActionMasked();
		
		if (actionCode == MotionEvent.ACTION_POINTER_UP || actionCode == MotionEvent.ACTION_UP)
		{
			int actionX = Api.getGraphicsSubsystem().pixelToDp((int)event.getX(event.getActionIndex()));
			int actionY = Api.getGraphicsSubsystem().pixelToDp((int)event.getY(event.getActionIndex()));
			
			player.shoot(BananaMath.getAngleDegrees( Api.getGraphicsSubsystem().getScreenWidth()/2, Api.getGraphicsSubsystem().getScreenHeight()/2,actionX,actionY));
		}
		
		return true; 
	}
	public void onDie()
	{
		Api.getCamera().stopFollowingEntity();
		Api.getCamera().setX(0);
		Api.getCamera().setY(0);
		
		Api.showJoystick(false);
	}
}