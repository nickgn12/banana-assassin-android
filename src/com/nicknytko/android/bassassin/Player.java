package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.Entity;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.engine.scene.BaseNode;
import com.nicknytko.android.bassassin.weapons.BaseWeapon;
import com.nicknytko.android.bassassin.weapons.Pistol;
import com.nicknytko.android.bassassin.weapons.Uzi;

public class Player extends Entity
{
	private BananaSprite sprite;
	public BaseWeapon currentWeapon;

	public boolean facingRight;
	private long lastAttackedTime;
	
	private boolean legacyShooting;
	
	public float aimAngle;
	public float recoil;
	public float recoilAdd;
	
	public float xspeed;
    public float yspeed;
    public float zspeed;
	
	public int health;
	private long deathTime;
	private boolean alive;

    public long getTimeDied(){ return deathTime; }
    public boolean isAlive(){ return alive; }

	public Player( boolean legacyShooting )
	{
		this.legacyShooting = legacyShooting;
	}
	
	public void loadGun( BaseWeapon newWep )
	{
		currentWeapon = newWep;

        sprite.loadWeapon(newWep);
		
		currentWeapon.ammoleft = currentWeapon.getDefaultAmmo();
	}
	
	public void shoot( float angle )
	{
		if (!alive){ return; }
        if (System.currentTimeMillis() < currentWeapon.lastShot + currentWeapon.cooldown()){ return; }

        currentWeapon.lastShot = System.currentTimeMillis();
		
		aimAngle = angle;
		recoilAdd += currentWeapon.getRecoil();
		
		Bullet b = new Bullet(x,y,0,0);
		b.owner = this;
		
		if (legacyShooting)
		{
			if (facingRight)
			{
				b.xspeed = 8;
				b.x = x + currentWeapon.bulletXOffset();
				b.y = y - height + currentWeapon.bulletYOffset();
			}
			else
			{
				b.xspeed = -8;
				b.x = x - currentWeapon.bulletXOffset();
				b.y = y - height + currentWeapon.bulletYOffset();
			}
		}
	
		Api.getGame().addProjectile(b);
	}
	
	public void initialize()
	{
        sprite = new BananaSprite(this);
        Api.getScene().addNode(sprite);
		
		width = sprite.banana.getWidth();
		height = sprite.banana.getHeight();
		x = Api.getGraphicsSubsystem().getScreenWidth()/2;
		y = Api.getGraphicsSubsystem().getScreenHeight()/2;

        sprite.x = x;
        sprite.y = y;
		
		xspeed = yspeed = zspeed = 0;
		
		facingRight = true;
		
		loadGun( new Uzi() );
		aimAngle = 0;
		recoil = 0;
		recoilAdd = 0;
		health = 100;
		lastAttackedTime = 0;
		alive = true;
		deathTime = 0;
	}
	public void onTick()
	{
		if (alive)
		{
			xspeed = (Api.getJoystick().getJoyX() * 50.0f);
			yspeed = (Api.getJoystick().getJoyY() * 50.0f);
		}
		else { xspeed = 0; yspeed = 0; }
		
		x += xspeed;
		y += yspeed;

        if (xspeed !=0 || yspeed != 0)
		
		recoil += recoilAdd*0.3;
		recoilAdd *= 0.7;
		recoil *= 0.5;
		
		if (legacyShooting)
		{
			if (Api.getJoystick().getJoyX() < 0){ facingRight = false; aimAngle = 180; }
			else if (Api.getJoystick().getJoyX() > 0){ facingRight = true; aimAngle = 0; }
		}
		else
		{
			if (aimAngle < -90 || aimAngle > 90){ facingRight = false; }
			else { facingRight = true; }
		}
		
		if (health <= 0 && alive)
		{
			alive = false;
			deathTime = System.currentTimeMillis();
		}
		
		if (health < 100 && alive)
		{
			if (System.currentTimeMillis() > lastAttackedTime+1500)
			{
				health += (System.currentTimeMillis() - lastAttackedTime+1500)/1000;
			}
		}
		if (health > 100){ health = 100; }
	}
	public void onCollide( Entity other )
	{
		if (other.getClass().equals(Zombie.class))
		{
			Zombie z = (Zombie)other;
			
			if (System.currentTimeMillis() > z.lastAttack+500 && z.isAlive())
			{
				health -= 8;
				z.lastAttack = System.currentTimeMillis();
				lastAttackedTime = System.currentTimeMillis();
			}
		}
	}
	public void onDie(){ sprite.dead = true; }
	public boolean canCollide(){ return true; }

    private class BananaSprite extends BaseNode
    {
        public boolean alive;
        public float aimAngle;
        public BananaBitmap banana;
        public BananaBitmap bananaLeft;
        public BananaBitmap shadow;
        public BananaBitmap gun;
        public BananaBitmap gunLeft;
        public BananaBitmap corpse;

        private Player parent;

        public BananaSprite( Player parent )
        {
            super();

            banana = Api.getGraphicsSubsystem().loadImage(R.drawable.banana);
            bananaLeft = Api.getGraphicsSubsystem().loadImage(R.drawable.bananaleft);
            shadow = Api.getGraphicsSubsystem().loadImage(R.drawable.shadow);
            corpse = Api.getGraphicsSubsystem().loadImage(R.drawable.bananadead);

            this.parent = parent;
        }

        public void loadWeapon( BaseWeapon w )
        {
            gun = Api.getGraphicsSubsystem().loadImage(w.getTexture());
            gunLeft = Api.getGraphicsSubsystem().loadImage(w.getLeftTexture());
        }

        public void onDraw( GraphicsSubsystem g)
        {
            x = parent.x;
            y = parent.y;
            z = parent.z;
            aimAngle = parent.aimAngle;

            g.drawBitmap(shadow, x - shadow.getWidth()/2 - Api.getCamera().getX(), y - shadow.getHeight()/2 - Api.getCamera().getY());

            if (parent.isAlive())
            {
                if (parent.facingRight)
                {
                    g.drawBitmap(banana, x - parent.width/2 - Api.getCamera().getX(), y - z - parent.height - Api.getCamera().getY());
                    g.drawRotatedBitmap(gun, x - parent.width/2 + parent.currentWeapon.xOffset() - Api.getCamera().getX(), y - parent.height/2 - z + parent.currentWeapon.yOffset() - Api.getCamera().getY(), aimAngle - parent.recoil,
                            parent.currentWeapon.getPivotX(), parent.currentWeapon.getPivotY());
                }
                else
                {
                    g.drawBitmap(bananaLeft, x - parent.width/2 - Api.getCamera().getX(), y - z - parent.height - Api.getCamera().getY());
                    g.drawRotatedBitmap(gunLeft, x - parent.width/2 + parent.currentWeapon.xLeftOffset() - Api.getCamera().getX(), y - parent.height/2 - z + parent.currentWeapon.yOffset() - Api.getCamera().getY(), (aimAngle+parent.recoil)-180,
                            parent.currentWeapon.getLeftPivotX(), parent.currentWeapon.getPivotY());
                }
            }
            else
            {
                g.drawBitmap(corpse, x - corpse.getWidth()/2, y - corpse.getHeight());
            }
        }
        public int getDepth(){ return y; }
    }
}