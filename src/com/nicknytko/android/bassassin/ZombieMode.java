package com.nicknytko.android.bassassin;

import android.view.MotionEvent;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.engine.JoystickButton;

public class ZombieMode extends Gamemode
{
	private boolean legacyShooting;
	private Player player;
	
	private JoystickButton attackButton;
	private JoystickButton grenadeButton;
	
	private int zombieDelay;
	private long lastZombieSpawn;
	
	private int zombieKills;
	
	public void spawnZombie( int x, int y )
	{
		Api.getGame().addEntity( new Zombie( x, y, player ));
	}
	
	public void initialize()
	{
		Api.showJoystick(true);
		
		attackButton = new JoystickButton(Api.getGraphicsSubsystem().getScreenWidth() - 133, Api.getGraphicsSubsystem().getScreenHeight() - 100,R.drawable.attack);
		grenadeButton = new JoystickButton(Api.getGraphicsSubsystem().getScreenWidth() - 100, Api.getGraphicsSubsystem().getScreenHeight() - 196,R.drawable.grenbutton);
		
		Api.getJoystick().addButton(attackButton);
		Api.getJoystick().addButton(grenadeButton);
		
		if (Api.getGlobalVars().getVariable("newshooting").getValue() == 0){ legacyShooting = true; }
		else { legacyShooting = false; }
		
		player = new Player(legacyShooting);
		Api.getGame().addEntity(player);
		
		zombieDelay = 800;
		lastZombieSpawn = System.currentTimeMillis() + zombieDelay;
		
		zombieKills = 0;
	}
	
	public void incrementKillCount( int zombieX, int zombieY )
	{
		zombieKills++;

        if (zombieKills % 10 == 0 && BananaMath.randInt(0,1000) < 750)
        {
            PowerUp p = new PowerUp(zombieX,zombieY);
            Api.getGame().addEntity(p);
        }
	}
	
	public void preDraw( GraphicsSubsystem g )
	{
		g.clearScreen(255, 255, 255);
	}
	
	public void onDraw( GraphicsSubsystem g )
	{
		g.setFontColor(0, 0, 0);
		g.setFontSize(33);
		if (zombieKills == 1){ g.drawText( 6, 26, "1 kill"); }
		else { g.drawText( 6, 26, zombieKills + " kills"); }
		
		/*
		float health = BananaMath.max(0, player.health);
		
		if (health < 100)
		{
			float alpha = BananaMath.min((1 - (health/100.0f))*255.0f,200);
			
			g.setColor(255, 0, 0, (int)alpha);
			g.drawRectangle(0, 0, g.getScreenWidth(), g.getScreenHeight());
		}
		*/
		
		g.setColor(0, 0, 0);
		g.drawRectangle(5, 45, 65, 20);
		
		if (player.health > 66)
			g.setColor(0, 255, 0);
		else if (player.health > 33)
			g.setColor(255,255,0);
		else
			g.setColor(255,0,0);
		
		float health = BananaMath.max(0, player.health);
		
		g.drawRectangle( 10, 50, (int)( (health / 100.0f) * 54.0f ), 10);
	}
	
	public void onTick()
	{
		if (legacyShooting)
		{
            if (player.currentWeapon.automatic() && attackButton.isDown)
            {
                player.shoot(player.aimAngle);
            }
            else if (attackButton.isPressed)
            {
                player.shoot(player.aimAngle);
            }

			if (grenadeButton.isPressed){  }
		}

        if (player.isAlive())
        {
            if (player.x < 0){ player.x = 0; }
            if (player.x > Api.getGraphicsSubsystem().getScreenWidth()){ player.x = Api.getGraphicsSubsystem().getScreenWidth(); }
            if (player.y < 0){ player.y = 0; }
            if (player.y > Api.getGraphicsSubsystem().getScreenHeight()){ player.y = Api.getGraphicsSubsystem().getScreenHeight(); }
        }

		if (!player.isAlive() && System.currentTimeMillis() > player.getTimeDied() + 3000)
		{
			Api.getGlobalVars().setVariable("score", zombieKills);
			Api.getGame().changeGamemode( new DeadScreen() );
		}
		
		if (System.currentTimeMillis() > lastZombieSpawn+zombieDelay)
		{
			--zombieDelay;
			spawnZombie( BananaMath.rand.nextInt(Api.getGraphicsSubsystem().getScreenWidth()), BananaMath.rand.nextInt(Api.getGraphicsSubsystem().getScreenHeight()) );
			lastZombieSpawn = System.currentTimeMillis();
		}
	}
	public boolean onTouch( MotionEvent event )
	{
		if (legacyShooting){ return true; }
		
		int actionCode = event.getActionMasked();
		
		if (actionCode == MotionEvent.ACTION_POINTER_UP || actionCode == MotionEvent.ACTION_UP)
		{
			int actionX = Api.getGraphicsSubsystem().pixelToDp((int)event.getX(event.getActionIndex()));
			int actionY = Api.getGraphicsSubsystem().pixelToDp((int)event.getY(event.getActionIndex()));
			
			player.shoot(BananaMath.getAngleDegrees(player.x, player.y,actionX,actionY));
		}
		
		return true; 
	}
	public void onDie()
	{
		Api.showJoystick(false);
	}
}