package com.nicknytko.android.bassassin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

import android.graphics.Typeface;
import android.view.MotionEvent;

public class Leaderboard extends Gamemode
{
	private Typeface font;
	private BananaBitmap ok;
	private BananaBitmap arrowup;
	private BananaBitmap arrowdown;
	private boolean enteringText;
	
	private char[] curname;
	private int slot = 4;
	private int currentScore;
	
	public String[] scoreNames;
	public int[] scoreValues;
	
	private final int magicInt = 71;
	
	public void save()
	{
		Api.getDebug().out("saving...");
		
		FileOutputStream out = Api.getFileIO().loadFileAsOutput("scores");
		try 
		{
			out.write( magicInt );
			
			for (int i=0;i < 5;i++)
			{
				out.write(scoreNames[i].charAt(0));
				out.write(scoreNames[i].charAt(1));
				out.write(scoreNames[i].charAt(2));
				int score = scoreValues[i];
				out.write( score & 0xFF );
				out.write( (score & 0xFF00) >> 8); //super fun fun bit-shifting time
			}
			out.write(96);
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		if (out != null) 
		{
			try 
			{
				out.close();
			} 
			catch (IOException e) {}
		}
	}
	
	public boolean load()
	{
		Api.getDebug().out("loading...");

		FileInputStream in = Api.getFileIO().loadFileAsInput("scores");
		if (in == null){ return false; }
		
		try 
		{
			int magic = in.read();
			Api.getDebug().out("Magic is: " + magic);
			
			if (magic != magicInt && magic != 70){ in.close(); save(); return false; }
			
			for (int i=0;i < 5;i++)
			{
				char name[] = new char[3];
				
				name[0] = (char)in.read();
				name[1] = (char)in.read();
				name[2] = (char)in.read();
				
				scoreNames[i] = new String(name,0,3);
				
				int score = in.read();
				if (magic == magicInt){ score += in.read() << 8; }
				
				scoreValues[i] = score;
			}
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		if (in != null) 
		{
			try 
			{
				in.close();
			} 
			catch (IOException e) {}
		}
		
		return true;
	}
	
	public void initialize()
	{
		ok = Api.getGraphicsSubsystem().loadImage(R.drawable.ok);
		arrowup = Api.getGraphicsSubsystem().loadImage(R.drawable.arrowup);
		arrowdown = Api.getGraphicsSubsystem().loadImage(R.drawable.arrowdown);
		
		font = Typeface.createFromAsset(Api.getMainActivity().getAssets(),"8bit.TTF");
		
		currentScore = Api.getGlobalVars().getVariable("score").getValue();
		Api.getGlobalVars().setVariable("score", 0);
		
		scoreNames  = new String[5];
		scoreValues = new int[5];
		
		if (!load())
		{
			scoreNames[0] = "GOD";
			scoreNames[1] = "BBQ";
			scoreNames[2] = "LOL";
			scoreNames[3] = "DUM";
			scoreNames[4] = "SUX";
			
			scoreValues[0] = 6000;
			scoreValues[1] = 250;
			scoreValues[2] = 175;
			scoreValues[3] = 100;
			scoreValues[4] = 0;
		}
		
		curname = new char[3];
		curname[0] = 'A';
		curname[1] = 'A';
		curname[2] = 'A';
		 
		if (currentScore > scoreValues[4] && currentScore != 0)
		{
			if (currentScore > scoreValues[4] && currentScore < scoreValues[3]){ slot = 4; }
			if (currentScore > scoreValues[3] && currentScore < scoreValues[2]){ slot = 3; }
			if (currentScore > scoreValues[2] && currentScore < scoreValues[1]){ slot = 2; }
			if (currentScore > scoreValues[1] && currentScore < scoreValues[0]){ slot = 1; }
			if (currentScore > scoreValues[0]){ slot = 0; }
			
			shift(slot);
			enteringText = true;
		}
		else
		{
			enteringText = false;
		}
	}
	
	public void shift( int upto )
	{
		for (int i=4; i >= upto+1;i--)
		{
			scoreValues[i] = scoreValues[i-1];
			scoreNames[i]  = scoreNames[i-1];
		}
	}
	
	
	public void onDraw( GraphicsSubsystem g )
	{
		g.clearScreen();
		g.setFontFace(font);
		
		if (enteringText)
		{
			for (int i=0;i<3;i++)
			{
				g.drawBitmap(arrowup,(int)(g.getScreenWidth()/2 - arrowup.getWidth()*1.5 + arrowup.getWidth()*i),g.getScreenHeight()/2 - arrowup.getHeight());
			}
			
			for (int i=0;i<3;i++)
			{
				g.drawBitmap(arrowdown,(int)(g.getScreenWidth()/2 - arrowup.getWidth()*1.5 + arrowup.getWidth()*i),g.getScreenHeight()/2 + arrowup.getHeight());
			}
			
			g.setFontColor(255, 255, 0);
			g.setFontSize(50);
			g.drawCenterText(g.getScreenWidth()/2, g.getScreenHeight()/2 - 80, "NEW HIGH SCORE");
			g.setFontSize(60);
			
			for (int i=0;i<3;i++)
			{
				g.drawCenterText(g.getScreenWidth()/2 + arrowup.getWidth()*(i-1), g.getScreenHeight()/2 + 50, new String(curname,i,1) );
			}
		}
		else
		{
			g.setFontSize(45);
			g.setFontColor(255, 255, 0);
			
			for (int i=0;i<5;i++)
			{
				g.drawCenterText(g.getScreenWidth()/2, 33, "HIGH SCORES");
				if (currentScore==scoreValues[i]){ g.setFontColor(255, 255, 255); }
				else { g.setFontColor(255, 255, 0); }

				g.drawCenterText( g.getScreenWidth()/2, 86 + 46*i, "-");
				g.drawCenterText( g.getScreenWidth()/2 - 25, 86 + 46*i, scoreNames[i].substring(2, 3));
				g.drawCenterText( g.getScreenWidth()/2 - 55, 86 + 46*i, scoreNames[i].substring(1, 2));
				g.drawCenterText( g.getScreenWidth()/2 - 85, 86 + 46*i, scoreNames[i].substring(0, 1));
				g.drawText( g.getScreenWidth()/2 + 10, 86 + 46*i, Integer.toString(scoreValues[i]) );
			}
		}
		g.drawBitmap(ok, g.getScreenWidth() - ok.getWidth(), g.getScreenHeight() - ok.getHeight());
	}
	
	public boolean onTouch( MotionEvent event )
	{
		if (event.getAction() != MotionEvent.ACTION_DOWN){ return false; }
		
		GraphicsSubsystem g = Api.getGraphicsSubsystem();
		int actionX = g.pixelToDp((int)event.getX());
		int actionY = g.pixelToDp((int)event.getY());
		
		if (BananaMath.pointIsInRect(g.getScreenWidth()-ok.getWidth(), g.getScreenHeight()-ok.getHeight(), ok.getWidth(), ok.getHeight(), actionX, actionY))
		{
			if (!enteringText)
			{
				Api.getGame().changeGamemode( new MainMenu() );
			}
			else 
			{ 
				enteringText = false; 
				scoreValues[slot] = currentScore; 
				scoreNames[slot] = new String(curname); 
				save();
			}
		}
		
		if (!enteringText){ return true; }
		
		for (int i=0;i<3;i++)
		{
			if (BananaMath.pointIsInRect((float)(g.getScreenWidth()/2 - arrowup.getWidth()*1.5 + arrowup.getWidth()*i),
								 g.getScreenHeight()/2 - arrowup.getHeight(),
								 arrowup.getWidth(),
								 arrowdown.getHeight(),
								 actionX,
								 actionY))
			{
				if (curname[i] == 65){ curname[i] = 90; }
				else { curname[i]--; }
			}
		}
		
		for (int i=0;i<3;i++)
		{
			if (BananaMath.pointIsInRect((int)(g.getScreenWidth()/2 - arrowup.getWidth()*1.5 + arrowup.getWidth()*i),
								 g.getScreenHeight()/2 + arrowup.getHeight(),
								 arrowup.getWidth(),
								 arrowdown.getHeight(),
								 actionX,
								 actionY))
			{
				if (curname[i] == 90){ curname[i] = 65; }
				else { curname[i]++; }
			}
		}
		
		return true;
	}
	
	public void onBackButton()
	{
		Api.getGame().changeGamemode(new MainMenu());
	}
}