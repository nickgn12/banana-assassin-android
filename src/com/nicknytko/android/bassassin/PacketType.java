package com.nicknytko.android.bassassin;

public enum PacketType
{
	NULL,
	CONNECT,
	ENTITY,
	NUMPLAYERS,
	PLAYERPOS,
	PLAYERCONNECTED,
	PLAYERNUM,
	PLAYERSHOOT,
	PLAYERDAMAGE,
	NEEDPACKET,
	LEVELDATA, //level name
	PLAYERDISCONNECTED,
	MAPDIMENSIONS,
	MAPDATA, //actual level data
	NEEDMAPDATA;
	
	public static int getNum( PacketType pt )
	{
		switch(pt)
		{
			case CONNECT:
				return 1;
			case ENTITY:
				return 2;
			case NUMPLAYERS:
				return 3;
			case PLAYERPOS:
				return 4;
			case PLAYERCONNECTED:
				return 5;
			case PLAYERNUM:
				return 6;
			case PLAYERSHOOT:
				return 7;
			case PLAYERDAMAGE:
				return 8;
			case NEEDPACKET:
				return 9;
			case LEVELDATA:
				return 10;
			case PLAYERDISCONNECTED:
				return 11;
			case MAPDIMENSIONS:
				return 12;
			case MAPDATA:
				return 13;
			case NEEDMAPDATA:
				return 14;
			case NULL:
			default:
				return 0;
		}
	}
	
	public static PacketType getType( int i )
	{
		switch(i)
		{
			case 1:
				return CONNECT;
			case 2:
				return ENTITY;
			case 3:
				return NUMPLAYERS;
			case 4:
				return PLAYERPOS;
			case 5:
				return PLAYERCONNECTED;
			case 6:
				return PLAYERNUM;
			case 7:
				return PLAYERSHOOT;
			case 8:
				return PLAYERDAMAGE;
			case 9:
				return NEEDPACKET;
			case 10:
				return LEVELDATA;
			case 11:
				return PLAYERDISCONNECTED;
			case 12:
				return MAPDIMENSIONS;
			case 13:
				return MAPDATA;
			case 14:
				return NEEDMAPDATA;
			case 0:
			default:
				return NULL;
		}
	}
};