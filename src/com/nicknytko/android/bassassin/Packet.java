package com.nicknytko.android.bassassin;

import java.net.DatagramPacket;

public class Packet
{
	public static String getPacketData( DatagramPacket p )
	{
		//return new String(p.getData());
		return new String(p.getData(),0,p.getLength()).trim();
	}
	
	public static String getRawPacketData( DatagramPacket p)
	{
		return new String(p.getData());
	}
	
	public static String createGenericPacket( PacketType pt )
	{
		String data = new String();
		data += (char)(32 + PacketType.getNum(pt));
		
		return data;
	}
	
	public static String createCustomPacket( PacketType pt, String data )
	{
		String base = createGenericPacket(pt);
		base += data;
		return base;
	}
	
	public static PacketType getPacketType( String str )
	{
		//System.out.println("Char: " + ((int)str.charAt(0)));
		return PacketType.getType((int)(str.charAt(0) - 32));
	}
	
	public static PacketType getPacketType( DatagramPacket p )
	{
		String type = new String(p.getData(),0,p.getLength()).trim();
		return getPacketType(type);
	}
	
	public static String createPlayerNumPacket( int num )
	{
		String base = createGenericPacket( PacketType.PLAYERNUM );
		base += num;
		
		return base;
	}
	
	public static String createNumPlayersPacket( PlayerEntry[] players )
	{
		String base = createGenericPacket( PacketType.NUMPLAYERS );
		//base += num;
		
		int numplayers = 0;
		
		for (int i=0;i < players.length;i++)
		{
			if (players[i].player != null)
			{
				numplayers += 1 << i;
			}
		}
		
		base += numplayers;
		
		return base;
	}
	
	public static String createPlayerMovePacket( int playernum, int x, int y, int z, int xspeed, int yspeed, int zspeed, int AimAngle )
	{
		String base = createGenericPacket( PacketType.PLAYERPOS );
		
		base += playernum;
		base += ';';
		base += x;
		base += ';';
		base += y;
		base += ';';
		base += z;
		base += ';';
		base += xspeed;
		base += ';';
		base += yspeed;
		base += ';';
		base += zspeed;
		base += ';';
		base += AimAngle;
		
		return base;
	}
	
	public static String createPlayerDisconnectPacket( int playernum )
	{
		String base = createGenericPacket( PacketType.NEEDPACKET );
		base += playernum;
		
		return base;
	}
	
	public static String createNeedInfoPacket( PacketType needed )
	{
		String base = createGenericPacket( PacketType.NEEDPACKET );
		base += (char)(PacketType.getNum(needed)+32);
		
		return base;
	}
	
	public static String createShootPacket( int playernum, int x, int y, int z, int xspeed, int yspeed )
	{
		String base = createGenericPacket( PacketType.PLAYERSHOOT );
		base += playernum;
		base += ';';
		base += x;
		base += ';';
		base += y;
		base += ';';
		base += z;
		base += ';';
		base += xspeed;
		base += ';';
		base += yspeed;
		base += ';';
		
		return base;
	}
	
	/*
	public static String createLevelDataPacket( Level l )
	{
		String base = createGenericPacket( PacketType.LEVELDATA );
		base += l.getName();
		
		return base;
	}
	*/
	
	public static String createMapDimensionsPacket( int w, int h )
	{
		String base = createGenericPacket( PacketType.MAPDIMENSIONS );
		base += w;
		base += ';';
		base += h;
		
		return base;
	}
	
	public static String createMapDataPacket( int y, String data )
	{
		String base = createGenericPacket( PacketType.MAPDATA );
		base += y;
		base += ';';
		base += data;
		
		System.out.println("createMapDataPacket returned string length of " + base.length());
		
		return base;
	}
	
	public static String createNeedMapDataPacket( int row )
	{
		String base = createGenericPacket( PacketType.NEEDMAPDATA );
		base += row;
		
		return base;
	}
	
	public static int getBasicPacketData( String s )
	{
		String temp = s.substring(1);
		return Integer.parseInt(temp);
	}
	
	public static int[] getArrayData( String s, int length )
	{
		String[] split = s.substring(1).split(";");
		int ret[] = new int[length];
		
		for (int i=0;i < length;i++)
		{
			ret[i] = Integer.parseInt(split[i]);
		}
		
		return ret;
	}
	
	public static int[] getPlayerMoveData( String s )
	{
		if (getPacketType(s) != PacketType.PLAYERPOS){ return null; }
		
		return getArrayData(s,8);
	}
	
	public static int[] getShootData( String s )
	{
		if (getPacketType(s) != PacketType.PLAYERSHOOT){ return null; }
		
		return getArrayData(s,6);
	}
	
	public static PacketType getPacketNeeded( String s )
	{
		if (getPacketType(s) != PacketType.NEEDPACKET){ return PacketType.NULL; }
		
		return getPacketType(s.substring(1));
	}
	
	public static String getLevelData( String s )
	{
		return s.substring(1);
	}
}