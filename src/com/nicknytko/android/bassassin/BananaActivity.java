package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;

import android.os.Bundle;
import android.view.KeyEvent;
import android.app.Activity;

public class BananaActivity extends Activity
{
	public static final float Version = 6f;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Api.registerMainActivity(this);
		Api.initEngine();
	}
	
	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event )
    {
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_BACK:
				Api.simulateBackButton();
				return true;
			default:
				return super.onKeyDown(keyCode, event);
		}
	}
	
	@Override
	public void onBackPressed()
	{
		Api.simulateBackButton();
	}
}
