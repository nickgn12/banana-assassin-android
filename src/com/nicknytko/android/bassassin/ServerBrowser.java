package com.nicknytko.android.bassassin;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Vector;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.MotionEvent;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaSocket;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class ServerBrowser extends Gamemode
{
	public boolean isOnWifi;
	private Beams beams;
	private BananaSocket sock;
	private InetAddress ip;
	private String baseIp;
	private int ipEnding;
	
	private boolean pingedServers;
	
	private Vector<InetAddress> serverIps;
	private Vector<Integer> serverPlayers;
	
	private long lastServerPing;
	
	public void initialize()
	{
		ConnectivityManager conManager = (ConnectivityManager)Api.getMainActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
		isOnWifi = netInfo.isConnected();
		beams = new Beams();
				
		serverIps = new Vector<InetAddress>();
		serverPlayers = new Vector<Integer>();
		
		lastServerPing = System.currentTimeMillis();
		
		if (isOnWifi)
		{
			ip = Api.getNet().getWifiIp();
			
			String ipString = ip.getHostAddress();
			
			int lastPeriod = -1;
			
			for (int i=ipString.length()-1;i > 0;i--)
			{
				if (ipString.charAt(i) == '.' && lastPeriod==-1)
				{
					lastPeriod = i;
					break;
				}
			}
			
			if (lastPeriod != -1)
				baseIp = ipString.substring(0, lastPeriod);
			else
				baseIp = ipString;
			
			ipEnding = Integer.parseInt( ipString.substring(lastPeriod+1) );
			sock = new BananaSocket();			
			sock.startListening();
		}
	}
	
	public void pingServers()
	{
		serverIps.clear();
		serverPlayers.clear();
		
		for (int i=1;i <= 255;i++)
		{
			if (i == ipEnding)
				continue;
			
			String ip = baseIp + "." + i;
						
			try {
				sock.sendPacket( Packet.createNeedInfoPacket(PacketType.NUMPLAYERS), InetAddress.getByName(ip), 6790);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void preDraw( GraphicsSubsystem g ){}
	public void onDraw( GraphicsSubsystem g )
	{
		beams.onDraw(g);
		
		if (!isOnWifi)
		{
			g.setFontColor(0, 0, 0);
			g.setFontSize(50);
			g.drawCenterText(g.getScreenWidth()/2, g.getScreenHeight()/2, "You must be on WI-FI to use multiplayer.");
			return;
		}
		
		if (serverIps.size() == 0)
		{
			g.setFontColor(0, 0, 0);
			g.setFontSize(50);
			g.drawCenterText(g.getScreenWidth()/2, g.getScreenHeight()/2, "Getting servers...");
		}
		else
		{
			g.setFontColor(0, 0, 0);
			g.setFontSize(35);
			g.drawCenterText(g.getScreenWidth()/2, 50, "Server List");
			
			for (int i=0;i < serverIps.size();i++)
			{	
				g.setColor(100, 100, 100, 100);
				g.drawRectangle(5, 70 + 50*i, g.getScreenWidth()-10, 45);
				g.setFontColor(0, 0, 0);
				g.setFontSize(20);
				
				g.drawText( 10, 100 + 50*i, serverIps.get(i).getHostName() );
				g.drawRightText( g.getScreenWidth() - 10, 100 + 50*i, serverPlayers.get(i) + "/15 players" );
			}
		}
	}
	public void onTick()
	{
		if (sock.running)
		{
			if (!pingedServers)
			{
				pingServers();
				pingedServers = true;
			}
			
			if (serverIps.size() == 0 && System.currentTimeMillis() > lastServerPing+500)
			{
				pingServers();
				lastServerPing = System.currentTimeMillis();
			}
			
			while (sock.packetsAvailable())
			{
				DatagramPacket dp = sock.getNextPacket();
				
				PacketType type = Packet.getPacketType(dp);
				String data = Packet.getPacketData(dp);
				
				if (type == PacketType.NUMPLAYERS)
				{
					serverIps.add(dp.getAddress());
					
					int players = 0;
					int numPlayers = Packet.getBasicPacketData(data);
					
					for (int i=0;i < 15;i++)
					{
						if ((numPlayers & (1 << i)) != 0)
						{
							players++;
						}
					}
					
					Api.getDebug().out("Server " + dp.getAddress().getHostName() + " has " + players + " players.");
					
					serverPlayers.add(players);
				}
			}
		}
	}
	public boolean onTouch( MotionEvent event )
	{
		if (isOnWifi)
		{
			if (serverIps.size() == 0){ return true; }
			
			//int actionX = Api.getGraphicsSubsystem().pixelToDp( (int)event.getX(event.getActionIndex()) );
			int actionY = Api.getGraphicsSubsystem().pixelToDp( (int)event.getY(event.getActionIndex()) );
			
			if (event.getActionMasked() == MotionEvent.ACTION_DOWN || event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN)
			{
				int selected = (actionY - 70) / 50;
				
				if (selected > serverIps.size()-1){ return true; }
				Api.getGame().changeGamemode( new Joining( serverIps.get(selected) ));
			}
			
			return true;
		}
		
		if (event.getActionMasked() == MotionEvent.ACTION_DOWN || event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN)
			Api.getGame().changeGamemode( new MainMenu() );
		
		return true;
	}
	public void onDie()
	{
		sock.stopListening();
	}
	
	public void onBackButton(){ Api.getGame().changeGamemode( new MainMenu() ); }
}