package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.Gamemode;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.menu.BasicMenuHandler;
import com.nicknytko.android.bassassin.menu.MenuHandler;
import com.nicknytko.android.bassassin.menu.MenuMain;
import com.nicknytko.android.bassassin.menu.TransitionMenuHandler;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;

public class MainMenu extends Gamemode
{
	private BananaBitmap imgBanana;
	private Typeface font;
	private Beams beams;
	public MenuHandler menu;	
	
	
	public void initialize()
	{
		this.imgBanana = Api.getGraphicsSubsystem().loadImage( R.drawable.menubanana );
				
		font = Typeface.createFromAsset(((Context)Api.getMainActivity()).getAssets(),"8bit.TTF");
		beams = new Beams();
		
		GraphicsSubsystem g = Api.getGraphicsSubsystem();
		
		menu = new BasicMenuHandler(new MenuMain(),g.getScreenWidth() - 250,g.getScreenHeight()/4);

        Api.showJoystick(false);
	}
	

	
	public void onTick()
	{
		if (menu.changeHandler != null){ menu = menu.changeHandler; } 
		menu.onTick();
	}
	
	@SuppressWarnings("unused")
	public void onDraw( GraphicsSubsystem g )
	{	
		beams.onDraw(g);
		g.drawBitmap( imgBanana, 0, g.getScreenHeight() - imgBanana.getHeight() );
		
		
		float buttonstart = ((float)g.getScreenHeight())/4;
		
		g.setDefaultFontFace();
		g.setFontSize(25);
		g.setFontColor(255, 255, 255);
				
		menu.onDraw(g);
		
		g.setFontColor(0, 0, 0);
		
		if (BananaActivity.Version % 10 == 0)
		{
			g.drawText(5,g.getScreenHeight()-30,"v" + BananaActivity.Version + ".0");
		}
		else
		{
			g.drawText(5,g.getScreenHeight()-30,"v" + BananaActivity.Version);
		}

		g.setFontFace(font);
		g.setFontSize(65);
		g.setFontStyle(Paint.Style.STROKE);
		g.setFontStrokeWidth(10);
		g.setFontColor(0, 0, 0);
		g.drawCenterText(g.getScreenWidth()/2, 80, "Banana Assassin");
		
		g.setFontSize(65);
		g.setFontColor(255, 255, 255);
		g.setFontStyle(Paint.Style.FILL);
		g.drawCenterText( g.getScreenWidth()/2, 80, "Banana Assassin");
	}
	
	public boolean onTouch( MotionEvent event)
	{
		return menu.onTouch(event);
	}
	
	public void onBackButton()
	{
		if (menu.getClass().equals(BasicMenuHandler.class) && menu.getMenu().getClass().equals(MenuMain.class))
		{
			Api.quit();
		}
		else if (menu.getClass().equals(BasicMenuHandler.class))
		{
			menu.changeHandler = new TransitionMenuHandler(menu.getMenu(),new MenuMain(),menu.x,menu.y);
		}
	}
}