package com.nicknytko.android.bassassin;

import java.net.InetAddress;

public class PlayerEntry
{
	public Player player;
	public InetAddress ip;
	public int port;
	public long lastPacketReceive;
	public boolean valid;
	public boolean connected;
	
	public PlayerEntry()
	{
		player = null;
		ip = null;
		port = 0;
		lastPacketReceive = System.currentTimeMillis() + 10000;
		valid = false;
		connected = false;
	}
	
	public PlayerEntry( Player p, InetAddress ip, int port )
	{
		player = p;
		this.ip = ip;
		this.port = port;
		valid = true;
		lastPacketReceive = System.currentTimeMillis() + 10000;
		connected = false;
	}
}