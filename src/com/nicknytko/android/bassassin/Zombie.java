package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.Entity;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.engine.Projectile;
import com.nicknytko.android.bassassin.engine.scene.BaseNode;

public class Zombie extends Entity
{
	//a rather dead fellow
	private float zspeed;
	
	public boolean direction;
	
	private boolean alive;
	private int ticks;
	private int health;

	public long lastAttack;
	private Player targetPlayer;

    private ZombieSprite sprite;

	public boolean isAlive(){ return alive; }
	
	public Zombie( int x, int y, Player targetPlayer )
	{
		this.x = x;
		this.y = y;
		this.targetPlayer = targetPlayer;
	}
	
	public void initialize()
	{
		direction = true;
		health = 100;
		alive = true;
		ticks = 0;
		z = Api.getGraphicsSubsystem().getScreenHeight();
		lastAttack = 0;
		
        sprite = new ZombieSprite(this);
		Api.getScene().addNode(sprite);

		width = sprite.zombie.getWidth();
		height = sprite.zombie.getHeight();
	}
	
	public void onTick( )
	{
		if (alive)
		{
			if (z == 0)
			{
				if (targetPlayer.y > y){ y += 1; }
				if (targetPlayer.y < y){ y -= 1; }
				
				if (targetPlayer.x < x){ x -= 1; direction = false; }
				if (targetPlayer.x > x){ x += 1; direction = true; }
			}
			
			z += zspeed;
			if (z > 0)
			{
				zspeed -= 1;
			}
			if (z < 0)
			{
				z = 0;
				zspeed = 0;
			}
			
			if (health <= 0){ alive = false; }
		}
		else
		{
			ticks++;
			if (ticks > 20){ dead = true; }
		}

        sprite.changed = true;
	}

	public void onCollide( Projectile other )
	{
		if (alive && z == 0)
		{
			alive = false;
			other.dead = true;
			
			((ZombieMode)Api.getGame().getGamemode()).incrementKillCount(x,y);
		}
	}

    public void onDie()
    {
        sprite.dead = true;
    }

	public boolean canCollide(){ return true; }

    private class ZombieSprite extends BaseNode
    {
        public BananaBitmap zombie;
        public BananaBitmap zombieLeft;
        public BananaBitmap shadow;
        public BananaBitmap corpse;

        private Zombie parent;

        public ZombieSprite( Zombie parent )
        {
            super();

            zombie = Api.getGraphicsSubsystem().loadImage(R.drawable.zombie);
            zombieLeft = Api.getGraphicsSubsystem().loadImage(R.drawable.zombieleft);
            shadow = Api.getGraphicsSubsystem().loadImage(R.drawable.shadow);
            corpse = Api.getGraphicsSubsystem().loadImage(R.drawable.zombiecorpse);

            this.parent = parent;
        }

        public void onDraw( GraphicsSubsystem g)
        {
            x = parent.x;
            y = parent.y;
            z = parent.z;

            g.drawBitmap(shadow, x - shadow.getWidth()/2, y - shadow.getHeight()/2 );

            if (parent.isAlive())
            {
                if (parent.direction)
                    g.drawBitmap( zombie, x - parent.width/2, (y-parent.z) - parent.height );
                else
                    g.drawBitmap( zombieLeft , x - parent.width/2, (y-z) - parent.height );
            }
            else
                g.drawBitmap( corpse, x - corpse.getWidth()/2, y - corpse.getHeight());
        }
        public int getDepth(){ return y; }
    }
}