package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.Entity;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.engine.scene.BaseNode;
import com.nicknytko.android.bassassin.weapons.Uzi;

public class PowerUp extends Entity
{
    private PowerUpSprite sprite;

    public int contains;
    private boolean canPickup;
    private float changeTime;
    private long nextChange;

    private float zspeed;

    public PowerUp( int x, int y )
    {
        this.x = x;
        this.y = y;

        z = 10;
        zspeed = 10;
    }

    public void initialize()
    {
        sprite = new PowerUpSprite(this);
        sprite.x = x;
        sprite.y = y;
        Api.getScene().addNode(sprite);

        width = sprite.pAmmo.getWidth();
        height = sprite.pAmmo.getHeight();

        contains = BananaMath.randInt(100,599);
        changeTime = BananaMath.rand.nextFloat() + 1.0f;
        nextChange = System.currentTimeMillis() + (long)changeTime;
    }
    public void onTick()
    {
        if (!canPickup)
        {
            z += zspeed;
            if (z > 0)
            {
                zspeed -= 1;
            }
            else if (z < 0)
            {
                z = 0;
                zspeed = 0;
            }
            if (System.currentTimeMillis() > nextChange)
            {
                changeTime *= 1.2f;

                if (changeTime > 1000){ canPickup = true; }
                else
                {
                    contains++;
                    if (contains > 5){ contains = 1; }

                    nextChange = System.currentTimeMillis() + (long)changeTime;
                }
            }
        }
        else
        {
            z += (30 - z)*0.5f;
        }
    }
    public void onDraw( GraphicsSubsystem g ){}
    public void onCollide( Entity other )
    {
        if (!canPickup){ return; }
        if (!(other instanceof  Player)){ return; }

        Player p = (Player)other;

        switch (contains)
        {
            case 1:
                break;
            case 2:
                p.loadGun( new Uzi() );
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }



        dead = true;
    }
    public void onDie(){ sprite.dead = true; }
    public boolean canCollide(){ return true; }

    private class PowerUpSprite extends BaseNode
    {
        public BananaBitmap pEarthQuake;
        public BananaBitmap pAmmo;
        public BananaBitmap pGrenade;
        public BananaBitmap pShotgun;
        public BananaBitmap pUzi;
        public BananaBitmap shadow;

        private PowerUp parent;

        public PowerUpSprite( PowerUp parent )
        {
            super();

            shadow = Api.getGraphicsSubsystem().loadImage(R.drawable.shadow);

            pEarthQuake = Api.getGraphicsSubsystem().loadImage(R.drawable.pearthquake);
            pAmmo = Api.getGraphicsSubsystem().loadImage(R.drawable.pammo);
            pGrenade = Api.getGraphicsSubsystem().loadImage(R.drawable.pgrenade);
            pShotgun = Api.getGraphicsSubsystem().loadImage(R.drawable.pshotgun);
            pUzi = Api.getGraphicsSubsystem().loadImage(R.drawable.puzi);

            this.parent = parent;
        }

        public void onDraw( GraphicsSubsystem g)
        {
            z = parent.z;

            g.drawBitmap(shadow, x - shadow.getWidth()/2, y - shadow.getHeight()/2 );

            switch (parent.contains)
            {
                case 1:
                    g.drawBitmap(pShotgun,x - parent.width/2, y - parent.height - z);
                    break;
                case 2:
                    g.drawBitmap(pUzi,x - parent.width/2, y - parent.height - z);
                    break;
                case 3:
                    g.drawBitmap(pAmmo,x - parent.width/2, y - parent.height - z);
                    break;
                case 4:
                    g.drawBitmap(pEarthQuake,x - parent.width/2, y - parent.height - z);
                    break;
                case 5:
                    g.drawBitmap(pGrenade,x - parent.width/2, y - parent.height - z);
                    break;
            }
        }
    }
}