package com.nicknytko.android.bassassin;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class Beams //draws the familiar orange and yellow menu beams
{
	float rotation;
	
	public void initialize()
	{
		rotation = (float)(Api.getTicks()) * 0.01745329251f;
		rotation -= BananaMath.floor( rotation / BananaMath.PI ) * BananaMath.PI;
	}
	
	public void drawTriangle( Canvas canvas, int x1, int y1, int x2, int y2, int x3, int y3, Paint paint)
	{
	    Path path = new Path();
	    path.setFillType(Path.FillType.EVEN_ODD);
	    path.moveTo(x1,y1);
	    path.lineTo(x2,y2);
	    path.lineTo(x3,y3);
	    path.lineTo(x1,y1);
	    path.close();

	    canvas.drawPath(path, paint);
	}
	
	public void drawBeams( GraphicsSubsystem g, int x, int y, int numbeams, float rotation )
	{
		float length = BananaMath.max(g.getActualScreenWidth(), g.getActualScreenHeight());
		Paint temppaint = new Paint();
		temppaint.setARGB(255,255,100,0);
		
		g.setColor(255, 255, 0);
		g.setAlpha(255);
		g.drawRectangle(0, 0, g.getScreenWidth(), g.getScreenHeight());
		
		float width =  (1.0f / ((float)numbeams)) * BananaMath.TWOPI;
		
		for (float i=0;i<numbeams;i+=2)
		{		
			float direction = (width*i) + rotation;
			
			
			drawTriangle(g.getCanvas(),
					(int)(x + Math.sin(direction)*length),(int)(y + Math.cos(direction)*length),
					x,y,
					(int)(x + Math.sin(direction + width)*length),(int)(y + Math.cos(direction + width)*length),
					temppaint);
					
		}
	}
	
	public void onDraw( GraphicsSubsystem g )
	{
		rotation += 0.01745329251f;
		if (rotation > BananaMath.TWOPI){ rotation = 0; }
		
		drawBeams( g, g.getActualScreenWidth()/2, g.getActualScreenHeight()/2, 16, rotation );
	}
}