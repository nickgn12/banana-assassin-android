package com.nicknytko.android.bassassin;

public class OtherPlayer extends Player
{	
	public OtherPlayer()
	{
		super(true);
	}

	public void onTick()
	{
		if (aimAngle < 90 || aimAngle > 270){ facingRight = true; }
		else { facingRight = false; }
		
		x += xspeed;
		y += yspeed;
	}
}