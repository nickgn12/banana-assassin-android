package com.nicknytko.android.bassassin.menu;

public class BaseMenu
{
	public BaseMenu setChange;
	
	public BaseMenu()
	{
		setChange = null;
	}
	
	public int		getButtonHeight(){ return 100; }//don't change this unless absolutely necessary	
	public int		getNumButtons(){ return 0; }
	
	public void		changeMenu( BaseMenu newmenu ){ setChange = newmenu; }
	
	public String	getMenuLabel( ){ return "BANANA ASSASSIN"; }
	//first button starts at 0
	public String	getButtonLabel( int button ){ return "NULL"; }
	public void		onButtonPress( int button ){ return; }
}