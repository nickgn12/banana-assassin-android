package com.nicknytko.android.bassassin.menu;

import com.nicknytko.android.bassassin.Leaderboard;
import com.nicknytko.android.bassassin.MainMenu;
import com.nicknytko.android.bassassin.engine.Api;

public class MenuMain extends BaseMenu
{
	public int		getButtonHeight(){ return 70; }
	public int		getNumButtons(){ return 3; }
	
	public String	getMenuLabel( ){ return "Banana Assassin"; }
	
	public String	getButtonLabel( int button )
	{
		switch (button)
		{
			case 0:
				return "New Game";
			case 1:
				return "High Scores";
			case 2:
				return "Store";
		}
		
		return super.getButtonLabel( button );
	}
	public void		onButtonPress( int button )
	{
		switch (button)
		{
			case 0:
				//Api.getGame().changeGamemode(new ZombieMode());
				MainMenu mm = (MainMenu)Api.getGame().getGamemode();
				mm.menu.changeHandler = new TransitionMenuHandler(mm.menu.getMenu(),new MenuModes(),mm.menu.x,mm.menu.y);
				break;
			case 1:
				Api.getGame().changeGamemode(new Leaderboard());
				break;
			case 2:
				break;
		}
	}
}