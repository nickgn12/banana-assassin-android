package com.nicknytko.android.bassassin.menu;

import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import android.view.MotionEvent;

public class MenuHandler
{
	public int x,y;
	protected BaseMenu menu;
	public MenuHandler changeHandler;
	
	public BaseMenu getMenu(){ return menu; }
	public void changeMenu( BaseMenu newmenu ){ menu = newmenu; }

	public void changeMenuHandler( MenuHandler newHandler ){ changeHandler = newHandler; }
	
	public MenuHandler( BaseMenu menu, int x, int y )
	{
		this.menu = menu;
		this.x = x;
		this.y = y;
		
		changeHandler = null;
	}
	
	public void onTick()
	{
		if (menu.setChange != null){ menu = menu.setChange; }
	}
	public boolean onTouch( MotionEvent event ){ return true; }
	public void onDraw( GraphicsSubsystem g ){}
}