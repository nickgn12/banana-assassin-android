package com.nicknytko.android.bassassin.menu;

import com.nicknytko.android.bassassin.ServerBrowser;
import com.nicknytko.android.bassassin.ZombieMode;
import com.nicknytko.android.bassassin.engine.Api;

public class MenuModes extends BaseMenu
{
	public int		getButtonHeight(){ return 70; }
	public int		getNumButtons(){ return 3; }
	
	public String	getMenuLabel( ){ return "Banana Assassin"; }
	
	public String	getButtonLabel( int button )
	{
		switch (button)
		{
			case 0:
				return "Classic";
			case 1:
				return "Host Game";
			case 2:
				return "Join Game";
		}
		
		return super.getButtonLabel( button );
	}
	public void		onButtonPress( int button )
	{
		switch (button)
		{
			case 0:
				Api.getGame().changeGamemode( new ZombieMode() );
				break;
			case 1:
				break;
			case 2:
				Api.getGame().changeGamemode( new ServerBrowser() );
				break;
		}
	}
}