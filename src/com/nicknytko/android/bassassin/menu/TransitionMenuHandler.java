package com.nicknytko.android.bassassin.menu;

import android.graphics.drawable.NinePatchDrawable;
import android.view.MotionEvent;

import com.nicknytko.android.bassassin.R;
import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class TransitionMenuHandler extends MenuHandler
{
	private BaseMenu newMenu;
	private float drawY;
	private NinePatchDrawable imgButton;
	
	public TransitionMenuHandler(BaseMenu menu, BaseMenu newMenu, int x, int y) 
	{
		super(menu, x, y);
		this.newMenu = newMenu;
		
		//drawY = Api.getGraphicsSubsystem().getScreenHeight();
		drawY = y;
		imgButton = (NinePatchDrawable) Api.getMainActivity().getResources().getDrawable(R.drawable.button);
	}
	
	private void drawButton( GraphicsSubsystem g, int x, int y, int w, int h, String text )
	{
		g.drawNinePatch(imgButton, x, y, w, h);
		g.setDefaultFontFace();
		g.setFontColor(255, 255, 255);
		g.setFontSize(30);
		g.drawText(x + w/8, y + h/2 + 10, text);
	}
	
	public void onTick()
	{
		super.onTick();
		
		drawY += (-(Api.getGraphicsSubsystem().getScreenHeight() - y) - drawY)*0.15f;

		if (Math.abs(drawY + (Api.getGraphicsSubsystem().getScreenHeight() - y)) < 1.5f){ this.changeHandler = new BasicMenuHandler(newMenu,x,y); }
	}
	public boolean onTouch( MotionEvent event ){ return true; }
	public void onDraw( GraphicsSubsystem g )
	{
		float buttonSpace = g.getScreenHeight() - y;
		
		for (int i=0;i<menu.getNumButtons();i++)
		{
			drawButton(g,x,(int) (drawY + ( ((float)i) / ((float)menu.getNumButtons()))*buttonSpace),200,menu.getButtonHeight(),menu.getButtonLabel(i));
		}
		
		for (int i=0;i<newMenu.getNumButtons();i++)
		{
			drawButton(g,x,(int) ((drawY+Api.getGraphicsSubsystem().getScreenHeight()) + ( ((float)i) / ((float)newMenu.getNumButtons()))*buttonSpace),200,newMenu.getButtonHeight(),newMenu.getButtonLabel(i));
		}
	}
}