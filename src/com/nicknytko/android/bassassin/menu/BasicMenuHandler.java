package com.nicknytko.android.bassassin.menu;

import android.graphics.drawable.NinePatchDrawable;
import android.view.MotionEvent;

import com.nicknytko.android.bassassin.R;
import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaMath;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;

public class BasicMenuHandler extends MenuHandler
{
	private NinePatchDrawable imgButton;
	
	public BasicMenuHandler(BaseMenu menu, int x, int y) 
	{
		super(menu, x, y);
		
		imgButton = (NinePatchDrawable) Api.getMainActivity().getResources().getDrawable(R.drawable.button);
	}
	
	private void drawButton( GraphicsSubsystem g, int x, int y, int w, int h, String text )
	{
		g.drawNinePatch(imgButton, x, y, w, h);
		g.setDefaultFontFace();
		g.setFontColor(255, 255, 255);
		g.setFontSize(30);
		g.drawText(x + w/8, y + h/2 + 10, text);
	}
	
	public void onTick(){ super.onTick(); }
	public boolean onTouch( MotionEvent event )
	{
		int actionmask = event.getAction() & MotionEvent.ACTION_MASK;
		if (actionmask != MotionEvent.ACTION_POINTER_DOWN && actionmask != MotionEvent.ACTION_DOWN){ return false; }
		
		int actionX = Api.getGraphicsSubsystem().pixelToDp((int)event.getX());
		int actionY = Api.getGraphicsSubsystem().pixelToDp((int)event.getY());
		
		float buttonstart = ((float)Api.getGraphicsSubsystem().getScreenHeight())/4;

		int numButtons = menu.getNumButtons();
		int buttonHeight = menu.getButtonHeight();
		for (int i=0;i<numButtons;i++)
		{
			if (BananaMath.pointIsInRect(Api.getGraphicsSubsystem().getScreenWidth() - 250,
											(int) (buttonstart + ( ((float)i) / ((float)numButtons))*((float)Api.getGraphicsSubsystem().getScreenHeight() - buttonstart)),
											200,buttonHeight,
											actionX,
											actionY))
			{
				menu.onButtonPress(i);
			}
		}
		
		return true;
	}
	public void onDraw( GraphicsSubsystem g )
	{
		for (int i=0;i<menu.getNumButtons();i++)
		{
			drawButton(g,x,(int) (y + ( ((float)i) / ((float)menu.getNumButtons()))*((float)g.getScreenHeight() - y)),200,menu.getButtonHeight(),menu.getButtonLabel(i));
		}
	}
}