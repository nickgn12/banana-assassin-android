package com.nicknytko.android.bassassin;

import com.nicknytko.android.bassassin.engine.Api;
import com.nicknytko.android.bassassin.engine.BananaBitmap;
import com.nicknytko.android.bassassin.engine.Entity;
import com.nicknytko.android.bassassin.engine.GraphicsSubsystem;
import com.nicknytko.android.bassassin.engine.Projectile;

public class Bullet extends Projectile
{
	private BananaBitmap image;
	public float xspeed, yspeed;
	private float ticks;
	
	public Bullet( int x, int y, float xspeed, float yspeed )
	{
		this.x = x;
		this.y = y;
		this.xspeed = xspeed;
		this.yspeed = yspeed;
	}
	
	public void initialize()
	{
		image = Api.getGraphicsSubsystem().loadImage(R.drawable.bullet);
		ticks = 0;
		
		width = 0;
		height = 0;
	}
	public void onTick()
	{
		x += xspeed;
		y += yspeed;
		
		ticks++;
		if (ticks > 500){ dead = true; }
	}
	public void onDraw( GraphicsSubsystem g )
	{
		g.drawBitmap(image, x - image.getWidth()/2, y - image.getHeight()/2);
	}
	public void onCollide( Entity other ){ }
	public void onDie(){}
}